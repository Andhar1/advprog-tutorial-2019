package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SauceTest {
    private Sauce mushroomSauce;
    private Sauce marinaraSauce;
    private Sauce plumTomatoSauce;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        mushroomSauce = new MushroomSauce();
        marinaraSauce = new MarinaraSauce();
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void mushroomSauceTestToString() {
        System.out.println(mushroomSauce);
        assertEquals("Mushroom Sauce".trim(),
                outContent.toString().trim());
    }

    @Test
    public void marinaraSauceTestToString() {
        System.out.println(marinaraSauce);
        assertEquals("Marinara Sauce".trim(),
                outContent.toString().trim());
    }

    @Test
    public void plumTomatoSauceTestToString() {
        System.out.println(plumTomatoSauce);
        assertEquals("Tomato sauce with plum tomatoes".trim(),
                outContent.toString().trim());
    }
}
