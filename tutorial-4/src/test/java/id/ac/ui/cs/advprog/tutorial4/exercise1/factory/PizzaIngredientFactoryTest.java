package id.ac.ui.cs.advprog.turorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class PizzaIngredientFactoryTest {

    private Class<?> pizzaIngredientFactoryClass;

    @Before
    public void setUp() throws Exception {
        pizzaIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.PizzaIngredientFactory");
    }

    @Test
    public void testPizzaIngredientFactoryIsInterface() {
        int classModifiers = pizzaIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
    }
    
    @Test
    public void testPizzaIngredientFactoryHasCreateDoughMethod() throws Exception {
        Method createDough = pizzaIngredientFactoryClass.getDeclaredMethod("createDough");
        int methodModifiers = createDough.getModifiers();
        

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", 
                createDough.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaIngredientFactoryHasCreateSauceMethod() throws Exception {
        Method createSauce = pizzaIngredientFactoryClass.getDeclaredMethod("createSauce");
        int methodModifiers = createSauce.getModifiers();
        

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", 
                createSauce.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaIngredientFactoryHasCreateCheeseMethod() throws Exception {
        Method createCheese = pizzaIngredientFactoryClass.getDeclaredMethod("createCheese");
        int methodModifiers = createCheese.getModifiers();
        

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", 
                createCheese.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaIngredientFactoryHasCreateVeggiesMethod() throws Exception {
        Method createVeggies = pizzaIngredientFactoryClass.getDeclaredMethod("createVeggies");
        int methodModifiers = createVeggies.getModifiers();
        

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", 
                createVeggies.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaIngredientFactoryHasCreateClamMethod() throws Exception {
        Method createClam = pizzaIngredientFactoryClass.getDeclaredMethod("createClam");
        int methodModifiers = createClam.getModifiers();
        

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", 
                createClam.getGenericReturnType().getTypeName());
    }

}