package id.ac.ui.cs.advprog.turorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {

    private Class<?> newYorkPizzaIngredientFactoryTest;

    @Before
    public void setUp() throws Exception {
        newYorkPizzaIngredientFactoryTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.NewYorkPizzaIngredientFactory");
    }

    @Test
    public void testNewYorkPizzaIngredientFactoryIsAIngredientFactory() {
        Class<?>[] parent = newYorkPizzaIngredientFactoryTest.getInterfaces();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.PizzaIngredientFactory", parent[0].getName());
    }

    @Test
    public void testNewYorkPizzaIngredientFactoryOverrideCreateDoughMethod() throws Exception {
        Method createDough = newYorkPizzaIngredientFactoryTest.getDeclaredMethod("createDough");
        int methodModifiers = createDough.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", 
                createDough.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testNewYorkPizzaIngredientFactoryOverrideCreateSauceMethod() throws Exception {
        Method createSauce = newYorkPizzaIngredientFactoryTest.getDeclaredMethod("createSauce");
        int methodModifiers = createSauce.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", 
                createSauce.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testNewYorkPizzaIngredientFactoryOverrideCreateCheeseMethod() throws Exception {
        Method createCheese = newYorkPizzaIngredientFactoryTest.getDeclaredMethod("createCheese");
        int methodModifiers = createCheese.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", 
                createCheese.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testNewYorkPizzaIngredientFactoryOverrideCreateVeggiesMethod() throws Exception {
        Method createVeggies = newYorkPizzaIngredientFactoryTest.getDeclaredMethod("createVeggies");
        int methodModifiers = createVeggies.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", 
                createVeggies.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testNewYorkPizzaIngredientFactoryOverrideCreateClamMethod() throws Exception {
        Method createClam = newYorkPizzaIngredientFactoryTest.getDeclaredMethod("createClam");
        int methodModifiers = createClam.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", 
                createClam.getGenericReturnType().getTypeName());
    }
}
