package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VeggiesTest {
    private Veggies blackOlives;
    private Veggies eggplant;
    private Veggies garlic;
    private Veggies mushroom;
    private Veggies onion;
    private Veggies redPepper;
    private Veggies spinach;
    private Veggies tomato;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
        tomato = new Tomato();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void blackOlivesTestToString() {
        System.out.println(blackOlives);
        assertEquals("Black Olives".trim(),
                outContent.toString().trim());
    }

    @Test
    public void eggplantTestToString() {
        System.out.println(eggplant);
        assertEquals("Eggplant".trim(),
                outContent.toString().trim());
    }

    @Test
    public void garlicTestToString() {
        System.out.println(garlic);
        assertEquals("Garlic".trim(),
                outContent.toString().trim());
    }

    @Test
    public void mushroomTestToString() {
        System.out.println(mushroom);
        assertEquals("Mushrooms".trim(),
                outContent.toString().trim());
    }

    @Test
    public void onionTestToString() {
        System.out.println(onion);
        assertEquals("Onion".trim(),
                outContent.toString().trim());
    }

    @Test
    public void redPepperTestToString() {
        System.out.println(redPepper);
        assertEquals("Red Pepper".trim(),
                outContent.toString().trim());
    }

    @Test
    public void spinachTestToString() {
        System.out.println(spinach);
        assertEquals("Spinach".trim(),
                outContent.toString().trim());
    }

    @Test
    public void tomatoTestToString() {
        System.out.println(tomato);
        assertEquals("Tomato".trim(),
                outContent.toString().trim());
    }
}
