package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {

    private static PizzaStore dStore;
    private PizzaStore nyStore;
    private Pizza pizza;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        dStore = new DepokPizzaStore();
        nyStore = new NewYorkPizzaStore();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void orderingClamPizzaTest() {
        pizza = dStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());

        pizza = nyStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }

    @Test
    public void orderingCheesePizzaTest() {
        pizza = dStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());

        pizza = nyStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void orderingVeggiePizzaTest() {
        pizza = dStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());

        pizza = nyStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void bakePizzaTest() {
        pizza = new ClamPizza(null);

        pizza.bake();
        assertEquals("Bake for 25 minutes at 350".trim(),
                outContent.toString().trim());
    }

    @Test
    public void cutPizzaTest() {
        pizza = new ClamPizza(null);

        pizza.cut();
        assertEquals("Cutting the pizza into diagonal slices".trim(),
                outContent.toString().trim());
    }

    @Test
    public void boxPizzaTest() {
        pizza = new ClamPizza(null);

        pizza.box();
        assertEquals("Place pizza in official PizzaStore box".trim(),
                outContent.toString().trim());
    }
}
