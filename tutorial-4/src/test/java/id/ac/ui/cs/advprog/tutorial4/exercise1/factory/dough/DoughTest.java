package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DoughTest {
    private Dough deepCrust;
    private Dough thickCrust;
    private Dough thinCrust;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        deepCrust = new DeepCrustDough();
        thickCrust = new ThickCrustDough();
        thinCrust = new ThinCrustDough();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void deepCrustDoughTestToString() {
        System.out.println(deepCrust);
        assertEquals("Deep Crust Dough".trim(),
                outContent.toString().trim());
    }

    @Test
    public void thickCrustDoughTestToString() {
        System.out.println(thickCrust);
        assertEquals("ThickCrust style extra thick crust dough".trim(),
                outContent.toString().trim());
    }

    @Test
    public void thinCrustDoughTestToString() {
        System.out.println(thinCrust);
        assertEquals("Thin Crust Dough".trim(),
                outContent.toString().trim());
    }
}
