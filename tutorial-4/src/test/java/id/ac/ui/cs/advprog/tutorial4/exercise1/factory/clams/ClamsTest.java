package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clams;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SteamedClams;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClamsTest {
    private Clams freshClams;
    private Clams frozenClams;
    private Clams steamedClams;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
        steamedClams = new SteamedClams();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void freshClamsTestToString() {
        System.out.println(freshClams);
        assertEquals("Fresh Clams from Long Island Sound".trim(),
                outContent.toString().trim());
    }

    @Test
    public void frozenClamsTestToString() {
        System.out.println(frozenClams);
        assertEquals("Frozen Clams from Chesapeake Bay".trim(),
                outContent.toString().trim());
    }

    @Test
    public void steamedClamsTestToString() {
        System.out.println(steamedClams);
        assertEquals("Steamed Clams".trim(),
                outContent.toString().trim());
    }
}
