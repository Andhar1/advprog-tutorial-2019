package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;


public class Main {
    public static void main(String[] args) {
        //Plain crusty sandwich
        Food order1 = new CrustySandwich();
        System.out.println(order1.getDescription());
        System.out.println(order1.cost());

        System.out.println();

        //Crusty sandwich with a variety of fillings
        order1 = new BeefMeat(order1);
        order1 = new Cheese(order1);
        order1 = new Lettuce(order1);
        order1 = new Cucumber(order1);
        order1 = new Tomato(order1);
        order1 = new TomatoSauce(order1);
        System.out.println(order1.getDescription());
        System.out.println(order1.cost());
    }
}
